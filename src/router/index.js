import Vue from 'vue';
import VueRouter from 'vue-router';
import Rhythm from '../views/Rhythm.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Rhythm',
    component: Rhythm,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
