# rhythm-generator

Repo moved to [https://github.com/gaetanHerve/rhythm-generator-v2](https://github.com/gaetanHerve/rhythm-generator-v2)

[TRY IT](https://gaetanherve.github.io/rhythm-generator-v2/)

## Purpose

Single page Web App diplaying randomly generated rhythmic sequences. Designed for music students.
This app uses [Vexflow 3](https://github.com/0xfe/vexflow), an open-source JavaScript library for rendering music notation created by Mohit Muthanna Cheppudira.
This is a beta version.


## Screenshots

<img src=./public/screenshots/rhythm-generator1.png alt="screenshot 1" height="200"/>
<img src=./public/screenshots/rhythm-generator2.png alt="screenshot 2" height="200"/>

## How to use rhythm-generator

Just choose your settings then click on "générer" button.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
